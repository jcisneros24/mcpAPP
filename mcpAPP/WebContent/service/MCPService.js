var services = angular.module('service', ['ngResource']);

services.factory('UserFactory', function ($resource) {
    return $resource('/MCPService/service/updateCode', {}, {
        query: {
            method: 'POST',
            params: {},
            isArray: false
        }
    })
});