var app = angular.module('MCPService', []);

app.controller('ServiceController', [ '$scope', 'UserFactory',
		function($scope, UserFactory) {
			UserFactory.post({}, function(userFactory) {
				$scope.firstname = userFactory.firstName;
			})
		} ]);